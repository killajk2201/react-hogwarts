import React from 'react';
import ReactTooltip from 'react-tooltip';

class Student extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			student: this.props.student,
		};
	}

	render() {
		return (
			<div className="student-card">
				<button
					className="delete-btn"
					data-tip="Delete"
					onClick={() => this.props.onClick(this.state.student.id)}
				>
					X
				</button>
				<ReactTooltip place="right" type="dark" effect="float">
					Delete Student
				</ReactTooltip>
				<img src={this.props.student.img} alt={this.props.student.name} />
				<div className="student-info">
					<h3>{this.props.student.name}</h3>
					<p>
						<strong>Identity:</strong> {this.props.student.identity}
					</p>
					<p>
						<strong>Age:</strong> {this.props.student.age}
					</p>
					<p>
						<strong>Best Friends:</strong> {this.props.student.bestFriend}
					</p>
				</div>
			</div>
		);
	}
}

export default Student;
