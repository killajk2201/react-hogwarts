import React from 'react';
import Student from './Student';
import path from 'path';

var students = [];
class HogwartsStudents extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			hogwartsStudents: students,
			name: '',
			identity: '',
			img: '',
			age: undefined,
			bestFriend: '',
		};
	}

	componentDidMount() {
		this.getStudents();
	}

	getStudents = () => {
		fetch('/api/students')
			.then(res => res.json())
			.then(students => this.setState({ hogwartsStudents: students }));
	};

	changeHandler = event => {
		console.log(event);
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	deleteStudentHandler = id => {
		fetch(path.join('/api/students/', id), {
			method: 'DELETE',
		})
			.then(() => {
				this.getStudents();
			})
			.catch(err => {
				console.error(err);
			});
	};

	addStudentHandler = event => {
		event.preventDefault();
		let newStudent = {
			name: this.state.name,
			identity: this.state.identity,
			img: this.state.img,
			age: this.state.age,
			bestFriend: this.state.bestFriend,
		};
		fetch('/api/students/add', {
			method: 'PUT',
			'content-type': 'application/json',
			body: JSON.stringify({
				newStudent: newStudent,
			}),
			headers: { 'Content-Type': 'application/json' },
		})
			.then(response => {
				console.log(response);
				this.setState({
					hogwartsStudents: [...this.state.hogwartsStudents, newStudent],
					name: '',
					identity: '',
					img: '',
					age: undefined,
					bestFriend: '',
				});
			})
			.catch(err => {
				console.error(err);
			});
	};

	render() {
		return (
			<div>
				<form>
					<input
						type="text"
						value={this.state.name}
						placeholder="name"
						name="name"
						onChange={this.changeHandler}
					/>
					<input
						type="text"
						value={this.state.identity}
						onChange={this.changeHandler}
						name="identity"
						placeholder="identity"
					/>
					<input
						type="text"
						value={this.state.img}
						placeholder="image url"
						name="img"
						onChange={this.changeHandler}
					/>
					<input
						type="text"
						value={this.state.age}
						onChange={this.changeHandler}
						name="age"
						placeholder="age"
					/>
					<input
						type="text"
						value={this.state.bestFriend}
						placeholder="best friend"
						name="bestFriend"
						onChange={this.changeHandler}
					/>
				</form>
				<button onClick={this.addStudentHandler}>Add Student</button>
				<h1>Hogwarts Students:</h1>

				<div className="class-list">
					{this.state.hogwartsStudents.map(student => (
						<Student student={student} onClick={this.deleteStudentHandler} />
					))}
				</div>
			</div>
		);
	}
}

export default HogwartsStudents;
